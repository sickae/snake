﻿namespace SnakeGame
{
    abstract class FixedElement : Element
    {
        public FixedElement(int x, int y, GameField field) : base(x, y, field) { }
    }
}
