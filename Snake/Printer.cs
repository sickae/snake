﻿using System;
using System.Text;

namespace SnakeGame
{
    static class Printer
    {
        static char[,] buffer;

        static Printer()
        {
            buffer = new char[Console.WindowWidth, Console.WindowHeight];
            Console.CursorVisible = false;
            Console.OutputEncoding = Encoding.Unicode;
        }

        static object obj = new Object();

        public static void Print(int x, int y, char character)
        {
            lock (obj)
            {
                if (buffer[x, y] != character)
                {
                    buffer[x, y] = character;
                    Console.SetCursorPosition(x, y);
                    Console.Write(character);
                }
            }
        }

        public static void Print(int x, int y, char character, ConsoleColor color)
        {
            lock (obj)
            {
                if (buffer[x, y] != character)
                {
                    buffer[x, y] = character;
                    ConsoleColor temp = Console.ForegroundColor;
                    Console.ForegroundColor = color;
                    Console.SetCursorPosition(x, y);
                    Console.Write(character);
                    Console.ForegroundColor = temp;
                }
            }
        }

        public static void Print(int x, int y, string text)
        {
            for (int i = 0; i < text.Length; i++)
                Print(x + i, y, text[i]);
        }

        public static void Print(int x, int y, string text, ConsoleColor color)
        {
            for (int i = 0; i < text.Length; i++)
                Print(x + i, y, text[i], color);
        }
    }
}
