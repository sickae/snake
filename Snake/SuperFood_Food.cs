﻿using System;

namespace SnakeGame
{
    class SuperFood : Food, IPrintable
    {
        public override ConsoleColor Color { get => ConsoleColor.Yellow; }
        public override int Value { get => 100; }

        public SuperFood(int x, int y, GameField field) : base(x, y, field) { }
    }
}
