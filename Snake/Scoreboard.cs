﻿using System;

namespace SnakeGame
{
    class Scoreboard
    {
        private GameField field;

        public Scoreboard(GameField field) => this.field = field;

        public void Display()
        {
            if (!field.GameOver)
            {
                if (!field.Pause)
                {
                    ConsoleColor color = Console.ForegroundColor;
                    if (field.Score == 666) color = ConsoleColor.DarkRed;
                    if (field.Score == 420) color = ConsoleColor.Green;
                    Printer.Print(field.Width + 2, 0, $"Score: {field.Score}   ");
                    Printer.Print(field.Width + 2, 1, "                        ");
                }
                else
                    Printer.Print(field.Width + 2, 0, "Paused    ", ConsoleColor.Yellow);
            }
            else
            {
                if (field.Won)
                {
                    Printer.Print(field.Width + 2, 0, "You won!    ", ConsoleColor.Green);
                    Printer.Print(field.Width + 2, 1, $"Score: {field.Score}   ");
                }
                else
                {
                    Printer.Print(field.Width + 2, 0, "You lost!   ", ConsoleColor.Red);
                    Printer.Print(field.Width + 2, 1, $"Score: {field.Score}   ");
                }
            }
        }
    }
}
