﻿using System;

namespace SnakeGame
{
    class Food : FixedElement, IPrintable
    {
        public char Form { get => '*'; }
        public virtual ConsoleColor Color { get => ConsoleColor.Red; }
        public virtual int Value { get => 50; }

        public Food(int x, int y, GameField field) : base(x, y, field) { }
    }
}
