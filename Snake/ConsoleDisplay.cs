﻿namespace SnakeGame
{
    class ConsoleDisplay
    {
        private IDisplayable source;

        public ConsoleDisplay(IDisplayable source) => this.source = source;

        public void Display()
        {
            IPrintable[] p = source.DisplayableElements();
            int[] size = source.DisplaySize;
            for (int i = 0; i < size[0]; i++)
            {
                for(int j = 0; j < size[1]; j++)
                {
                    IPrintable e = null;
                    for (int k = 0; k < p.Length; k++)
                        if (p[k].X == i && p[k].Y == j) e = p[k];
                    if (e != null) Printer.Print(e.X, e.Y, e.Form, e.Color);
                    else Printer.Print(i, j, ' ');
                }
            }
        }
    }
}
