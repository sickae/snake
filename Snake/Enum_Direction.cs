﻿namespace SnakeGame
{
    enum Direction
    {
        NONE,
        LEFT,
        RIGHT,
        UP,
        DOWN
    }
}
