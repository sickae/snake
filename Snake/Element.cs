﻿namespace SnakeGame
{
    abstract class Element
    {
        private int x;
        private int y;
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        protected GameField field;

        public Element(int x, int y, GameField field)
        {
            this.x = x;
            this.y = y;
            this.field = field;
            field.AddElement(this);
        }
    }
}
