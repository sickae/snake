﻿using System;

namespace SnakeGame
{
    class Wall : FixedElement, IPrintable
    {
        public char Form { get => '█'; }
        public ConsoleColor Color { get => ConsoleColor.White; }

        public Wall(int x, int y, GameField field) : base(x, y, field) { }
        }
}
