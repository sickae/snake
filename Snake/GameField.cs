﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SnakeGame
{
    class GameField : IDisplayable
    {
        static Random rnd = new Random();
        private bool gameOver;
        private bool won;
        private bool pause;
        private bool hasSuperfood;
        private int width;
        private int height;
        private int score;
        private List<Element> elements;
        private Snake snake;
        private List<int[]> foodPositions;
        private List<int[]> positions;
        public int Width { get => width; }
        public int Height { get => height; }
        public int Score { get => score; set => score = value; }
        public int[] DisplaySize { get => new int[] { width, height }; }
        public bool GameOver { get => gameOver; set => gameOver = value; }
        public bool Won { get => won; set => won = value; }
        public bool Pause { get => pause; set => pause = value; }
        public bool HasSuperFood { get => hasSuperfood; set => hasSuperfood = value; }
        public Element[] Elements { get => elements.ToArray(); }
        public Snake Snake { get => snake; }
        private static object obj = new object();

        public GameField(int width, int height)
        {
            this.width = width;
            this.height = height;
            score = 0;
            gameOver = false;
            won = false;
            pause = false;
            hasSuperfood = false;
            elements = new List<Element>();
            snake = new Snake((int)Math.Ceiling(width / 2.0), (int)Math.Ceiling(height / 2.0), this);
            positions = new List<int[]>(width * height);
            foodPositions = positions;
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    positions.Add(new int[] { i, j });
            GenerateWalls();
            CalculateFreePositions();
            GenerateFood();
        }

        public void AddElement(Element element)
        {
            lock (obj)
            {
                elements.Add(element);
                if (foodPositions != null && foodPositions.Count > 0)
                    foodPositions.Remove(foodPositions.FirstOrDefault(f => f[0] == element.X && f[1] == element.Y));
            }
        }

        public void RemoveElement(Element element)
        {
            lock (obj)
            {
                if (element is Food && !(element is SuperFood))
                    GenerateFood();
                elements.Remove(element);
            }
        }

        public IPrintable[] DisplayableElements()
        {
            List<IPrintable> pl = new List<IPrintable>();
            for (int i = 0; i < elements.Count; i++)
                if (elements[i] is IPrintable) pl.Add(elements[i] as IPrintable);
            return pl.ToArray();
        }

        void GenerateFood()
        {
            CalculateFreePositions();
            if (foodPositions.Count > 0)
            {
                int idx = rnd.Next(0, foodPositions.Count - 1);
                new Food(foodPositions[idx][0], foodPositions[idx][1], this);
            }
            else
            {
                gameOver = true;
                won = true;
            }
        }

        public void GenerateSuperFood()
        {
            CalculateFreePositions();
            if (foodPositions.Count > 0)
            {
                int idx = rnd.Next(0, foodPositions.Count - 1);
                new SuperFood(foodPositions[idx][0], foodPositions[idx][1], this);
                hasSuperfood = true;
            }
            else
            {
                gameOver = true;
                won = true;
            }
        }

        public void RemoveSuperFood()
        {
            lock (obj)
            {
                elements.Remove(elements.FirstOrDefault(e => e is SuperFood));
                hasSuperfood = false;
            }
        }

        void GenerateWalls()
        {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    if (i == 0 || i == width - 1 || j == 0 || j == height - 1) {
                        Wall w = new Wall(i, j, this);
                        Printer.Print(i, j, w.Form);
                    }
        }

        public void CalculateFreePositions()
        {
            lock(obj)
            {
                foodPositions = new List<int[]>();
                foreach (int[] pos in positions)
                    if (!elements.Any(e => e.X == pos[0] && e.Y == pos[1]))
                        foodPositions.Add(pos);
            }
        }

        public Element ElementAt(int x, int y)
        {
            lock(obj)
                return elements.FirstOrDefault(e => e.X == x && e.Y == y);
        }
    }
}
