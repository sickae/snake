﻿using System;

namespace SnakeGame
{
    interface IPrintable
    {
        int X { get; }
        int Y { get; }
        char Form { get; }
        ConsoleColor Color { get; }
    }
}
