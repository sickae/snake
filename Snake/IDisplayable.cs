﻿namespace SnakeGame
{
    interface IDisplayable
    {
        int[] DisplaySize { get; }

        IPrintable[] DisplayableElements();
    }
}
