﻿using System.Collections.Generic;

namespace SnakeGame
{
    class Snake
    {
        private int lastx;
        private int lasty;
        private int toadd;
        private List<Body> snake;
        private GameField field;
        private Direction direction;
        public List<Body> Body { get => snake; }
        public Direction Direction { get => direction; set => direction = value; }

        public Snake(int x, int y, GameField field)
        {
            this.field = field;
            snake = new List<Body>();
            snake.Add(new Body(x, y, field, true));
            lastx = x;
            lasty = y;
            direction = Direction.NONE;
        }

        public void Eat(Food food)
        {
            toadd++;
            if (food is SuperFood)
                toadd++;
            field.RemoveElement(food);
            field.Score += food.Value + field.Snake.Body.Count - 1;
        }

        public void Move()
        {
            int dx = 0;
            int dy = 0;
            switch (direction)
            {
                case Direction.LEFT: dx = -1; break;
                case Direction.RIGHT: dx = 1; break;
                case Direction.UP: dy = -1; break;
                case Direction.DOWN: dy = 1; break;
                default: return;
            }
            int prevx = snake[0].X + dx;
            int prevy = snake[0].Y + dy;
            for (int i = 0; i < snake.Count; i++)
            {
                int tempx = snake[i].X;
                int tempy = snake[i].Y;
                snake[i].Move(prevx, prevy);
                lastx = prevx;
                lasty = prevy;
                prevx = tempx;
                prevy = tempy;
            }
            if (toadd > 0)
            {
                snake.Add(new Body(prevx, prevy, field));
                toadd--;
            }
        }
    }
}
