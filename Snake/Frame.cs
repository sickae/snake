﻿using System;
using System.Diagnostics;
using System.Threading;

namespace SnakeGame
{
    class Frame
    {
        const int FIELD_WIDTH = 50;
        const int FIELD_HEIGHT = 20;
        const int SPEED = 150;
        const int MAX_SPEED = 30;
        const int SPEED_INC = 10;
        private int currentspeed;
        private int tempspeed;
        private bool playing;
        private GameField field;
        Timer updateTimer;
        Timer superfoodTimer;
        Timer scoreTimer;
        Stopwatch sw;
        ConsoleDisplay display;
        Scoreboard scoreboard;
        Direction direction;
        ConsoleKey prevkey;

        public Frame()
        {
            field = new GameField(FIELD_WIDTH, FIELD_HEIGHT);
            direction = Direction.NONE;
            currentspeed = SPEED;
            playing = true;
        }

        public void Run()
        {
            tempspeed = currentspeed;
            sw = new Stopwatch();
            sw.Start();
            display =  new ConsoleDisplay(field);
            scoreboard = new Scoreboard(field);
            updateTimer = new Timer(Update, null, 0, currentspeed);
            scoreTimer = new Timer(LowerScore, null, 0, 300);
            superfoodTimer = new Timer(UpdateSuperFood, null, 5000, 5000);
            while(playing)
            {
                if(sw.ElapsedMilliseconds > 0)
                {
                    SetSpeed(currentspeed);
                    sw.Restart();
                }
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (prevkey != key.Key)
                {
                    prevkey = key.Key;
                    sw.Restart();
                }
                if (field.GameOver)
                {
                    SetSpeed(currentspeed);
                    updateTimer?.Dispose();
                    scoreTimer?.Dispose();
                    superfoodTimer?.Dispose();
                    Thread.Sleep(500);
                    Restart();
                }
                switch (key.Key)
                {
                    case ConsoleKey.LeftArrow:
                        if (!field.Pause)
                        {
                            direction = field.Snake.Direction == Direction.RIGHT ? Direction.RIGHT : Direction.LEFT;
                            if (field.Snake.Direction == Direction.LEFT && sw.ElapsedMilliseconds > 0)
                                ChangeTempSpeed(-SPEED_INC);
                            else if (currentspeed != tempspeed)
                                SetSpeed(currentspeed);
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        if (!field.Pause)
                        {
                            direction = field.Snake.Direction == Direction.LEFT ? Direction.LEFT : Direction.RIGHT;
                            if (field.Snake.Direction == Direction.RIGHT && sw.ElapsedMilliseconds > 0)
                                ChangeTempSpeed(-SPEED_INC);
                            else if (currentspeed != tempspeed)
                                SetSpeed(currentspeed);
                        }
                        break;
                    case ConsoleKey.UpArrow:
                        if (!field.Pause)
                        {
                            direction = field.Snake.Direction == Direction.DOWN ? Direction.DOWN : Direction.UP;
                            if (field.Snake.Direction == Direction.UP && sw.ElapsedMilliseconds > 0)
                                ChangeTempSpeed(-SPEED_INC);
                            else if (currentspeed != tempspeed)
                                SetSpeed(currentspeed);
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        if (!field.Pause)
                        {
                            direction = field.Snake.Direction == Direction.UP ? Direction.UP : Direction.DOWN;
                            if (field.Snake.Direction == Direction.DOWN && sw.ElapsedMilliseconds > 0)
                                ChangeTempSpeed(-SPEED_INC);
                            else if (currentspeed != tempspeed)
                                SetSpeed(currentspeed);
                        }
                        break;
                    case ConsoleKey.Escape:
                        playing = false;
                        break;
                    case ConsoleKey.Spacebar:
                        field.Pause = !field.Pause;
                        break;
                    case ConsoleKey.Add:
                        ChangeSpeed(-SPEED_INC);
                        break;
                    case ConsoleKey.Subtract:
                        ChangeSpeed(SPEED_INC);
                        break;
                }
            }
        }

        private static object obj = new object();
        public void Update(Object state)
        {
            lock (obj)
            {
                if (!field.Pause && !field.GameOver)
                {
                    field.Snake.Direction = direction;
                    field.Snake.Move();
                    display.Display();
                }
                //else if(field.GameOver)
                //{
                //    SetSpeed(currentspeed);
                //    updateTimer?.Dispose();
                //    scoreTimer?.Dispose();
                //    superfoodTimer?.Dispose();
                //}
                scoreboard.Display();
            }
        }

        public void UpdateSuperFood(Object state)
        {
            lock (obj)
            {
                if (!field.HasSuperFood)
                    field.GenerateSuperFood();
                else
                    field.RemoveSuperFood();
            }
        }

        public void LowerScore(Object state)
        {
            if (!field.GameOver && field.Score > 0)
                field.Score--;
        }

        void Restart()
        {
            field = new GameField(FIELD_WIDTH, FIELD_HEIGHT);
            playing = true;
            direction = Direction.NONE;
            Run();
        }

        void ChangeSpeed(int modifier)
        {
            if (currentspeed + modifier >= MAX_SPEED)
            {
                currentspeed += modifier;
                updateTimer?.Change(0, currentspeed);
            }
        }

        void ChangeTempSpeed(int modifier)
        {
            if (tempspeed + modifier >= MAX_SPEED)
            {
                tempspeed += modifier;
                updateTimer?.Change(0, tempspeed);
            }
        }

        void SetSpeed(int speed)
        {
            lock (obj)
            {
                updateTimer?.Change(0, speed > MAX_SPEED ? speed : MAX_SPEED);
                currentspeed = speed;
                tempspeed = speed;
            }
        }
    }
}
