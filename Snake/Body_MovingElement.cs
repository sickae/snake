﻿using System;

namespace SnakeGame
{
    class Body : MovingElement, IPrintable
    {
        private bool head;
        public char Form { get => head ? 'O' : 'o'; }
        public ConsoleColor Color { get => head ? ConsoleColor.Cyan : ConsoleColor.Green; }

        public int[] DisplaySize => throw new NotImplementedException();

        public Body(int x, int y, GameField field) : base(x, y, field)
        {
            head = false;
        }

        public Body(int x, int y, GameField field, bool isHead) : base(x, y, field)
        {
            head = isHead;
        }

        static private object obj = new object();
        public override void Collide(Element element)
        {
            lock (obj)
            {
                if ((element is Body || element is Wall) && field.Snake.Body[field.Snake.Body.Count - 1] != element)
                    field.GameOver = true;
                else if (element is Food)
                    field.Snake.Eat(element as Food);
            }
        }
    }
}
