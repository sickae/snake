﻿using System;

namespace SnakeGame
{
    abstract class MovingElement : Element
    {
        public MovingElement(int x, int y, GameField field) : base(x, y, field) { }

        public abstract void Collide(Element element);

        static object obj = new Object();
        public void Move(int x, int y)
        {
            Element e = field.ElementAt(x, y);
            if (e != null) Collide(e);
            if (field.GameOver && !field.Won) return;
            X = x;
            Y = y;
        }
    }
}
